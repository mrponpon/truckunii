package mymodels

type (
	TruckProfile struct {
		ID       int    `db:"id" json:"id"`
		Username string `db:"username" json:"username"`
		Address  string `db:"address" json:"address"`
		Contact  string `db:"contact" json:"contact"`
		Vehicle  string `db:"vehicle" json:"vehicle"`
		Status   string `db:"status" json:"status"`
	}
	TruckRegisterProfile struct {
		Username string `db:"username" json:"username"`
		Address  string `db:"address" json:"address"`
		Contact  string `db:"contact" json:"contact"`
		Vehicle  string `db:"vehicle" json:"vehicle"`
		Status   string `db:"status" json:"status"`
	}
	TruckUpdateProfile struct {
		ID      int    `db:"id" json:"id"`
		Address string `db:"address" json:"address"`
		Contact string `db:"contact" json:"contact"`
		Vehicle string `db:"vehicle" json:"vehicle"`
		Status  string `db:"status" json:"status"`
	}
	TruckWorkProfile struct {
		ID           int `db:"id" json:"id"`
		TruckId      int `db:"truck_id" json:"truck_id"`
		PromotionId  int `db:"promotion_id" json:"promotion_id"`
		CheckpointId int `db:"checkpoint_id" json:"checkpoint_id"`
		StockId      int `db:"stock_id" json:"stock_id"`
	}
	Checkpoint struct {
		ID     int    `db:"id" json:"id"`
		Status string `db:"status" json:"status"`
	}
	PromotionCode struct {
		ID   int    `db:"id" json:"id"`
		Code string `db:"code" json:"code"`
	}
	StockRecycle struct {
		ID           int    `db:"id" json:"id"`
		RecycleId    int    `db:"recycle_id" json:"recycle_id"`
		PromotionId  int    `db:"promotion_id" json:"promotion_id"`
		Location     string `db:"location" json:"location"`
		Weight       string `db:"weight" json:"weight"`
		Shippingcost string `db:"shipping_cost" json:"shipping_cost"`
	}
	RecycleProfile struct {
		ID       int    `db:"id" json:"id"`
		Username string `db:"username" json:"username"`
		Address  string `db:"address" json:"address"`
		Contact  string `db:"contact" json:"contact"`
		Status   string `db:"status" json:"status"`
	}
)

func (TruckProfile) TableName() string {
	return "truck"
}
