package myvar

const (
	// QP = qurey param
	QPTruckID           = "truck_id"
	QPTruckIDMissingMsg = "Missing truck id."
)
const (
	MsgSuccess        = "Success"
	MsgFail           = "Oop. Something wrong please try again later."
	MsgTypeParamWrong = "type parameter wrong."
)

const (
	MsgNotFoundData = "not found data"
)
