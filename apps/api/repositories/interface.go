package repositories

import "truck-unii-app/apps/myconfig/mymodels"

type Repositories struct {
}

type RepoTruck interface {
	FindTruckProfile(truckID int) (mymodels.TruckProfile, error)
	FindTruckProfileByUsername(truckUsername string) (mymodels.TruckProfile, error)
	InsertTruckProfile(req *mymodels.TruckRegisterProfile) error
	UpdateTruckProfile(req *mymodels.TruckUpdateProfile) error
}
