package repositories

import (
	"truck-unii-app/apps/myconfig/mymodels"
	"truck-unii-app/pkg/myfunc"

	"gorm.io/gorm"
)

type rTruck struct {
	DB *gorm.DB
}

func NewRepoTruck(db *gorm.DB) RepoTruck {
	return &rTruck{
		DB: db,
	}
}
func (r *rTruck) FindTruckProfileByUsername(truckUsername string) (mymodels.TruckProfile, error) {
	query := `
	SELECT
		"tp"."id",
		"tp"."address",
		"tp"."contact",
		"tp"."vehicle",
		"tp"."status"

	FROM "truck" "tp"
	WHERE "tp"."username" = $1;`
	truck_profile := new(mymodels.TruckProfile)
	if err := r.DB.Raw(query, truckUsername).Scan(truck_profile).Error; err != nil {
		return *truck_profile, myfunc.MyErrFormat(err)
	}
	return *truck_profile, nil
}

func (r *rTruck) FindTruckProfile(truckID int) (mymodels.TruckProfile, error) {
	query := `
	SELECT
		"tp"."id",
		"tp"."username",
		"tp"."address",
		"tp"."contact",
		"tp"."vehicle",
		"tp"."status"

	FROM "truck" "tp"
	WHERE "tp"."id" = $1;`
	truck_profile := new(mymodels.TruckProfile)
	if err := r.DB.Raw(query, truckID).Scan(truck_profile).Error; err != nil {
		return *truck_profile, myfunc.MyErrFormat(err)
	}
	return *truck_profile, nil
}

func (r *rTruck) InsertTruckProfile(req *mymodels.TruckRegisterProfile) error {
	query := `
		INSERT INTO "truck" (
			"username",
			"address",
			"contact",
			"vehicle",
			"status"
		)
		VALUES
			($1, $2, $3, $4, $5)
		RETURNING "id";`
	if err := r.DB.Exec(query, req.Username, req.Address, req.Contact, req.Vehicle, req.Status).Error; err != nil {
		return myfunc.MyErrFormat(err)
	}
	return nil

}

func (r *rTruck) UpdateTruckProfile(req *mymodels.TruckUpdateProfile) error {
	query := `
		UPDATE "truck" SET 
			"address" = $2,
			"contact" = $3,
			"vehicle" = $4,
			"status" = $5
		WHERE "id" = $1;`
	if err := r.DB.Exec(query, req.ID, req.Address, req.Contact, req.Vehicle, req.Status).Error; err != nil {
		return myfunc.MyErrFormat(err)
	}
	return nil
}

func (r *rTruck) FindTruckWork(truckID int) (*mymodels.TruckWorkProfile, error) {
	return nil, nil
}
