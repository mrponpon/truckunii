package services

import (
	"truck-unii-app/apps/api/repositories"
	"truck-unii-app/apps/myconfig/mymodels"
	"truck-unii-app/apps/myconfig/myvar"
)

type sTruck struct {
	rt repositories.RepoTruck
}

func NewServiceTruck(rt repositories.RepoTruck) ServiceTruck {
	return &sTruck{
		rt: rt,
	}
}

func (s *sTruck) TruckProfileGet(truckID int) (interface{}, string, error) {
	var failMsg string
	mydata, err := s.rt.FindTruckProfile(truckID)
	if err != nil {
		return nil, failMsg, err
	}
	if mydata.ID == 0 {
		failMsg = myvar.MsgNotFoundData
		return nil, failMsg, nil
	}
	return mydata, failMsg, nil
}

func (s *sTruck) InsertTruckProfile(req *mymodels.TruckRegisterProfile) (*mymodels.TruckProfile, error) {
	err := s.rt.InsertTruckProfile(req)
	if err != nil {
		return nil, err
	}
	truck_profile, err := s.rt.FindTruckProfileByUsername(req.Username)
	if err != nil {
		return nil, err
	}
	return &truck_profile, nil
}

func (s *sTruck) UpdateTruckProfile(req *mymodels.TruckUpdateProfile) (*mymodels.TruckProfile, error) {
	data, err := s.rt.FindTruckProfile(req.ID)
	if err != nil {
		return nil, err
	}
	if req.Address != "" {
		data.Address = req.Address
	}
	if req.Contact != "" {
		data.Contact = req.Contact
	}
	if req.Vehicle != "" {
		data.Vehicle = req.Vehicle
	}
	if req.Status != "" {
		data.Status = req.Status
	}
	truck_update_profile := mymodels.TruckUpdateProfile{
		ID:      data.ID,
		Address: data.Address,
		Contact: data.Contact,
		Vehicle: data.Vehicle,
		Status:  data.Status,
	}

	err = s.rt.UpdateTruckProfile(&truck_update_profile)
	if err != nil {
		return nil, err
	}
	truck_profile, err := s.rt.FindTruckProfile(req.ID)
	if err != nil {
		return nil, err
	}
	return &truck_profile, nil
}
