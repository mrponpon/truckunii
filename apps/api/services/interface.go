package services

import "truck-unii-app/apps/myconfig/mymodels"

type ServiceTruck interface {
	TruckProfileGet(truckID int) (interface{}, string, error)
	InsertTruckProfile(req *mymodels.TruckRegisterProfile) (*mymodels.TruckProfile, error)
	UpdateTruckProfile(req *mymodels.TruckUpdateProfile) (*mymodels.TruckProfile, error)
}
