CREATE TABLE "truck" (
  "id" int PRIMARY KEY,
  "username" varchar UNIQUE,
  "address" varchar,
  "contact" varchar,
  "vehicle" varchar,
  "status" varchar
);

CREATE TABLE "work" (
  "id" int PRIMARY KEY,
  "truck_id" int,
  "promotion_id" int,
  "checkpoint_id" int,
  "stock_id" int
);

CREATE TABLE "checkpoint" (
  "id" int PRIMARY KEY,
  "status" varchar
);

CREATE TABLE "sign_images" (
  "id" int PRIMARY KEY,
  "filename" varchar,
  "url" varchar,
  "work_id" int
);

CREATE TABLE "promotion" (
  "id" int PRIMARY KEY,
  "code" varchar
);

CREATE TABLE "recycle" (
  "id" int PRIMARY KEY,
  "username" varchar UNIQUE,
  "address" varchar,
  "contact" varchar,
  "status" varchar
);

CREATE TABLE "stock" (
  "id" int PRIMARY KEY,
  "recycle_id" int,
  "promotion_id" int,
  "location" varchar,
  "weight" varchar,
  "shipping_cost" int
);

CREATE TABLE "product_stock" (
  "id" int PRIMARY KEY,
  "stock_id" int,
  "product" jsonb
);

ALTER TABLE "work" ADD FOREIGN KEY ("truck_id") REFERENCES "truck" ("id");

ALTER TABLE "work" ADD FOREIGN KEY ("promotion_id") REFERENCES "promotion" ("id");

ALTER TABLE "work" ADD FOREIGN KEY ("checkpoint_id") REFERENCES "checkpoint" ("id");

ALTER TABLE "work" ADD FOREIGN KEY ("stock_id") REFERENCES "stock" ("id");

ALTER TABLE "sign_images" ADD FOREIGN KEY ("work_id") REFERENCES "work" ("id");

ALTER TABLE "stock" ADD FOREIGN KEY ("recycle_id") REFERENCES "recycle" ("id");

ALTER TABLE "stock" ADD FOREIGN KEY ("promotion_id") REFERENCES "promotion" ("id");

ALTER TABLE "product_stock" ADD FOREIGN KEY ("stock_id") REFERENCES "stock" ("id");
